from random import randrange
#==========================================
t = [' '] * 9
#==========================================
def check_winner(n):
    if (t[0]==t[1]==t[2]==n) or (t[3]==t[4]==t[5]==n) or (t[6]==t[7]==t[8]==n):
        return True
    if (t[0]==t[3]==t[6]==n) or (t[1]==t[4]==t[7]==n) or (t[2]==t[5]==t[8]==n):
        return True
    if (t[0]==t[4]==t[8]==n) or (t[2]==t[4]==t[6]==n):
        return True
    return False
#==========================================
def checkplay():
    if check_winner('X') == True:
        print('Winner')
        return True
    elif check_winner('O') == True:
        print('Looser')
        return True
    else:
        return False
#==========================================
def draw():
    print(' --- --- ---')
    print(f'| {t[0]} | {t[1]} | {t[2]} |')
    print(' --- --- ---')
    print(f'| {t[3]} | {t[4]} | {t[5]} |')
    print(' --- --- ---')
    print(f'| {t[6]} | {t[7]} | {t[8]} |')
    print(' --- --- ---')
#==========================================
def check_input(x):
    if '0' <= x <= '8':
        if t[int(x)] == ' ' : return True
        else                : return False
    else:
        return False
#==========================================
def computer():
    while True:
        a = input('Enter (0 to 8) : ')
        if check_input(a) == True:
            break
    t[int(a)] = 'X'
#==========================================
def random():
    while True:
        x = randrange(0,9)
        if t[x] == ' ':
            t[x] = 'O'
            break
#==========================================
def attack():
    for i in range(9):
        if t[i] == ' ':
            t[i] = 'O'
            if check_winner('O') == True:
                return True
            t[i] = ' '
    return False
#==========================================
def defence():
    for i in range(9):
        if t[i] == ' ':
            t[i] = 'X'
            if check_winner('X') == True:
                t[i] = 'O'
                return True
            t[i] = ' '
    return False
#==========================================
def AI():
    if  attack() == False :
        if defence() == False :
            random()
#==========================================
while True:
    draw()
    if checkplay() == True : break
    computer()
    draw()
    if checkplay() == True : break
    AI()
print('GAME OVER!')